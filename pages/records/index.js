import React, { useEffect, useState } from "react";
	import { InputGroup, Form, FormControl, Button, Card, Row, Col } from "react-bootstrap";

	export default function Records() {

		const [allTransaction, setAllTransaction] = useState([])
		const [savings, setSavings] = useState([])
		const [allExpense, setAllExpense] = useState([]);
		const [allIncome, setAllIncome] = useState([]);
		const [categoryType, setCategoryType] = useState("All");

		useEffect(() => {
			let token = localStorage.getItem("token");
			fetch("https://nameless-lowlands-74481.herokuapp.com/api/ledger/",{
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllTransaction(data);

				data.map(data => {
					if(data.type === 'Income'){
						setAllIncome(allIncome => [...allIncome, data])
					}else{
						setAllExpense(allExpense => [...allExpense, data])
					}
				})

				fetch("https://nameless-lowlands-74481.herokuapp.com/api/users/details/",{
				headers: {
					"Authorization": `Bearer ${token}`
				}
				})
				.then(res => res.json())
				.then(data => {
					setSavings(data.savings)
				})
			})
		}, [])


		const aTransaction = allTransaction.slice(0).reverse().map(data => {
			// const ListOfCurrentTransaction = allTransaction.filter(value => value.dateOfTransaction <= data.dateOfTransaction && value.type === data.type);

			let dateObject = new Date(data.createdON);
			let dateString = JSON.stringify(dateObject.toUTCString());
			dateString = dateString.substring(1, dateString.length - 13);

			if (data.type === 'Income'){

				// const totalIncome = ListOfCurrentTransaction.reduce((a, b) => +a + +b.amount, 0);


				return(
					<Card key={data._id} className='mt-3'>
					<Card.Body>
						<Row>
							<Col className="col-6">
								<h5>{data.description}</h5>
								<h6>
									<span className="text-success">Income</span>
									<p>{dateString}</p>
								</h6>
								
							</Col>
							<Col className="col-6 text-right">
								<h6 className="text-success">+ {data.amount.toLocaleString(undefined, { minimumFractionDigits: 2 })}</h6>
							</Col>
						</Row>
					</Card.Body>
					</Card>
				)
			}else{
				
				// const totalExpense = ListOfCurrentTransaction.reduce((a, b) => +a + +b.amount, 0);
				
				return(
					<Card key={data._id} className='mt-3'>
						<Card.Body>
							<Row>
								<Col className="col-6">
									<h5>{data.description}</h5>
									<h6>
										<span className="text-danger">Expense</span>
										<p>{dateString}</p>	
									</h6>
									
								</Col>
								<Col className="col-6 text-right">
									<h6 className="text-danger"> - {data.amount.toLocaleString(undefined, { minimumFractionDigits: 2 })}</h6>
									
								</Col>
							</Row>
						</Card.Body>
					</Card>
				)
			}	
		})

		let aIncome = allIncome.slice(0).reverse().map(data => {

		 	let dateObject = new Date(data.createdON);
			let dateString = JSON.stringify(dateObject.toUTCString());
			dateString = dateString.substring(1, dateString.length - 13);

			return(
			<Card key={data._id} className='mt-3'>
			<Card.Body>
				<Row>
					<Col className="col-6">
						<h5>{data.description}</h5>
						<h6>
							<span className="text-success">Income</span>
							<p>{dateString}</p>
						</h6>
					</Col>
					<Col className="col-6 text-right">
						<h6 className="text-success">+ {data.amount.toLocaleString(undefined, { minimumFractionDigits: 2 })}</h6>
					</Col>
				</Row>
			</Card.Body>
			</Card>
			)
		})

		let aExpense = allExpense.slice(0).reverse().map(data => {

			let dateObject = new Date(data.createdON);
			let dateString = JSON.stringify(dateObject.toUTCString());
			dateString = dateString.substring(1, dateString.length - 13);

			return(
					<Card key={data._id} className='mt-3'>
						<Card.Body>
							<Row>
								<Col className="col-6">
									<h5>{data.description}</h5>
									<h6>
										<span className="text-danger">Expense</span>
										<p>{dateString}</p>	
									</h6>
									
								</Col>
								<Col className="col-6 text-right">
									<h6 className="text-danger"> - {data.amount.toLocaleString(undefined, { minimumFractionDigits: 2 })}</h6>
									
								</Col>
							</Row>
						</Card.Body>
					</Card>
				)
		})

		return (
			<React.Fragment>
			<div className="row">
				<h1 className="col-md-12 col-lg-6">Records</h1>
				<h1 className="col-md-12 col-lg-6 text-right">Total Savings: {savings.toLocaleString()}</h1>
			</div>
				<InputGroup>
					<InputGroup.Prepend>
						<Button variant="success" type="submit" id="submitBtn" href="/records/addRecord" size="lg">
							Add
						</Button>
					</InputGroup.Prepend>
					<FormControl
						placeholder="Search Record"
					/>
					<Form.Group controlId="type">
						<Form.Control as="select" onChange={(e) => setCategoryType(e.target.value)} required>
							<option>All</option>
							<option>Income</option>
							<option>Expense</option>
						</Form.Control>
					</Form.Group>
				</InputGroup>
				
				{categoryType == 'All'
				?
					aTransaction
				:
				categoryType == 'Income'
				?
					aIncome
				:
					aExpense
				}

				
			</React.Fragment>
		);
	}
