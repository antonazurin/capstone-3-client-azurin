import React, {useState, useEffect} from 'react'
import {Form, Button, Card} from 'react-bootstrap'
import Swal from 'sweetalert2';
import Router from 'next/router';

export default function index() {

	const [categoryName, setCategoryName] = useState("")
	const [categoryType, setCategoryType] = useState("")
	const [isActive, setIsActive] = useState(true)

	useEffect(() => {
		if (categoryName !== "" && categoryType !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [categoryName, categoryType]);

	function addCategory(e) {
		e.preventDefault();

		let token = localStorage.getItem("token");
		fetch("https://nameless-lowlands-74481.herokuapp.com/api/categories/", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`,
			},
			body: JSON.stringify({
				name: categoryName,
				type: categoryType,
			}),
		})
			.then(res => res.json())
			.then(data => {
				if (data) {
					Swal.fire({
						icon: "success",
						title: "Category Added!",
						text: "Successfully created a category",
					});

					Router.push("/category");
				} else {
					Swal.fire({
						icon: "error",
						title: "Add Category Failed!",
						text: "There has been an internal server error.",
					});
				}
			});

		setCategoryName("");
		setCategoryType("");
	}


	return(
		<React.Fragment>
			<h1>New Category</h1>
			<Card>
				<Card.Header>Category Information</Card.Header>
				<Card.Body>
					<Form onSubmit={e => addCategory(e)}>
						<Form.Group>
							<Form.Label>Category Name:</Form.Label>
							<Form.Control
								type="text"
								value={categoryName}
								onChange={e => setCategoryName(e.target.value)}
								placeholder="Enter category name"
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Category Type:</Form.Label>
							<Form.Control
								as="select"
								value={categoryType}
								onChange={e => setCategoryType(e.target.value)}
								required
							>
								<option disabled value="">
									Select Category
								</option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</Form.Group>
						{isActive ? (
							<Button
								variant="primary"
								type="submit"
								id="submitBtn"
								className="btn-block"
							>
								Submit
							</Button>
						) : (
							<Button
								variant="danger"
								type="submit"
								id="submitBtn"
								className="btn-block"
								disabled
							>
								Submit
							</Button>
						)}
					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
		)
}
