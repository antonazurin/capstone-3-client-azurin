import React, { useEffect, useState,  useContext } from "react";
import {Table, Button, Card} from 'react-bootstrap';
import UserContext from '../../UserContext';
import Router from 'next/router';

export default function index(){

	const [allCategory, setAllCategory] = useState([]);
	

	useEffect(() => {
		let token = localStorage.getItem("token");
		fetch("https://nameless-lowlands-74481.herokuapp.com/api/categories/",{
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllCategory(data);
		})
	}, [])

	console.log(allCategory)

	const aCategory = allCategory.map(data => {
		return(
			<tr key={data._id}>
				<td>{data.name}</td>
				<td>{data.type}</td>
			</tr>
			)
	})

	return(
			<React.Fragment>
			<Card>
				<Card.Body>
					<h1>Categories</h1>
					<Button
						variant="outline-success"
						href = "/addCategory"
					>
					Add Category
					</Button>
					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Category Name</th>
								<th>Category Type</th>
							</tr>
						</thead>
						<tbody>
							{aCategory}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
			</React.Fragment>
	)
}