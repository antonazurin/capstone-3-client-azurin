import React from 'react'
import {Container, Row, Col, Card} from 'react-bootstrap'

export default function index(){
	return(
		<React.Fragment>
		<br />
		<Card bg="dark" text="light" style={{ width: '30rem' }} className="mx-auto">
		<Card.Body>
		<Card.Title>About</Card.Title>
		<Card.Text>
		This is a basic financial tracker app that lets user record their 
		money. Helps when they need to keep track of their spending.
		</Card.Text>
		</Card.Body>
		</Card>
		</React.Fragment>
	)
}